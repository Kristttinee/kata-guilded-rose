MAXIMUM_QUALITY = 50
DEGRADES_TWICE = 2

class Default():
    
    def __init__(self,quality,sell_date,conjured):
        self.quality = quality
        self.sell_date = sell_date
        self.conjured = conjured 

    def days_gone(self,days):
        start_quality = self.quality
        if self.__has_sell_date_passed(days):
            self.__quality_degrades_twice(days)
            if self.quality <= 0:
                self.quality = 0
        else: 
            self.quality -= days 
        
        if self.conjured == True:
            quality_gone = start_quality - self.quality
            self.quality = start_quality - (quality_gone * DEGRADES_TWICE)                 

    def get_value(self,days):
        if self.quality > MAXIMUM_QUALITY:
            self.quality = MAXIMUM_QUALITY   
        return self.quality
    
    def __has_sell_date_passed(self,days):
        return self.sell_date < days

    def __quality_degrades_twice(self,days):
        quality_before_sell_date_ends = self.quality - self.sell_date 
        days_after_sell_date_ends = days - self.sell_date 
      
        quality_before_sell_date_ends -= days_after_sell_date_ends * DEGRADES_TWICE

        self.quality = quality_before_sell_date_ends