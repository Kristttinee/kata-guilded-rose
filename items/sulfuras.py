from items.default import Default

LEGENDARY_QUALITY = 80


class Sulfuras(Default):
    
    def days_gone(self,days):
        self.quality = LEGENDARY_QUALITY
    
    def get_value(self,days):
        return self.quality
        