from items.default import Default

INCREASES_TWICE = 2
INCREASES_THREE_TIMES = 3
TEN_DAYS = 10
FIVE_DAYS = 5 

class BackstagePasses(Default):
    
    def days_gone(self,days): 
        sell_days_left = self.sell_date - days 

        if sell_days_left > TEN_DAYS:
            self.quality += days  

        if sell_days_left <= TEN_DAYS and sell_days_left > FIVE_DAYS:
            days_outside_ten = max(self.sell_date - TEN_DAYS,0)
            days_inside_ten = days - days_outside_ten 

            self.quality += days_inside_ten * INCREASES_TWICE 
            self.quality += days_outside_ten 
        
        if sell_days_left <= FIVE_DAYS:
            days_outside_five = max(self.sell_date - FIVE_DAYS,0)
            days_inside_five = days - days_outside_five

            self.quality += days_inside_five * INCREASES_THREE_TIMES
            self.quality += days_outside_five
        
        if sell_days_left < days: 
            self.quality = 0