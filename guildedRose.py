MAXIMUM_QUALITY = 50

class GuildedRose():
   
    def __init__(self,item):
        self.item = item 

    def days_gone(self,days):
        self.item.days_gone(days)  
    
    def get_value(self,days):
        quality = self.item.get_value(days)

        return quality