from expects import *
import mamba
from guildedRose import GuildedRose
from items.default import Default 
from items.agedBrie import AgedBrie
from items.sulfuras import Sulfuras
from items.backstagePasses import BackstagePasses

with description("Kata Guilded Rose") as self:
    with it("When a day passes the quality degrades"):
        quality = 50
        sell_date = 10 
        days = 5
        final_quality = 45
        conjured = False 

        default = Default(quality,sell_date,conjured)
        guildedRose = GuildedRose(default)
        
        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality)) 

    with it("Once the sell by date has passed, quality degrades twice as fast"):
        quality = 50
        sell_date = 10 
        days = 20
        final_quality = 20
        conjured = False 

        default = Default(quality,sell_date,conjured)
        guildedRose = GuildedRose(default)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))         

    with it("The quality of an item is never negative"):
        quality = 20
        sell_date = 10 
        days = 20
        final_quality = 0
        conjured = False 

        default = Default(quality,sell_date,conjured)
        guildedRose = GuildedRose(default)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))    

    with it("The quality of an item is never more than 50"):
        quality = 60
        sell_date = 0
        days = 0
        final_quality = 50
        conjured = False 

        default = Default(quality,sell_date,conjured)
        guildedRose = GuildedRose(default)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  

    with it('"Aged Brie" actually increases in quality the older it gets'):
        quality = 30
        sell_date = 10
        days = 5
        final_quality = 35
        conjured = False 

        agedBrie = AgedBrie(quality,sell_date,conjured)
        guildedRose = GuildedRose(agedBrie)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  
    

    with it('"Sulfuras", being a legendary item, never has to be sold or decreases in quality'):
        quality = 45
        sell_date = 0
        days = 10
        final_quality = 80
        conjured = False

        sulfuras = Sulfuras(quality,sell_date,conjured)
        guildedRose = GuildedRose(sulfuras)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  


    with it('"Backstage passes": quality increases by 1 when there are more than 10 days'):
        quality = 10
        sell_date = 20
        days = 5
        final_quality = 15
        conjured = False 

        backstagePasses = BackstagePasses(quality,sell_date,conjured)
        guildedRose = GuildedRose(backstagePasses)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  

    with it('"Backstage passes": quality increases by 2 when there are 10 days or less '):
        quality = 5
        sell_date = 9
        days = 3
        final_quality = 11
        conjured = False 

        backstagePasses = BackstagePasses(quality,sell_date,conjured)
        guildedRose = GuildedRose(backstagePasses)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  

    with it('"Backstage passes": quality increases by 3 when there are 5 days or less'):
        quality = 10
        sell_date = 6
        days = 3
        final_quality = 17
        conjured = False 

        backstagePasses = BackstagePasses(quality,sell_date,conjured)
        guildedRose = GuildedRose(backstagePasses)

        guildedRose.days_gone(days)  

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  

    with it('"Backstage passes": quality drops to 0 after the concert'):
        quality = 10
        sell_date = 6
        days = 7
        final_quality = 0
        conjured = False 

        backstagePasses = BackstagePasses(quality,sell_date,conjured)
        guildedRose = GuildedRose(backstagePasses)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  

    with it('"Conjured" items degrade in quality twice as fast as normal items'):
        quality = 10
        sell_date = 1
        days = 2
        final_quality = 4
        conjured = True    

        default = Default(quality,sell_date,conjured)
        guildedRose = GuildedRose(default)

        guildedRose.days_gone(days)

        result = guildedRose.get_value(days)
        expect(result).to(equal(final_quality))  